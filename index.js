/* bài 1
input: người dùng nahạp 3 số nguyên

bước 1: tạo 3 biến 1,2,3
bước 2: lấy dữ liệu người dùng
bước 3: sắp xếp theo thứ tự và in ra

output: xếp theo thứ tự */

function sapXep() {
  var soNhat = document.getElementById("txt-so-thu-nhat").value * 1;
  var soHai = document.getElementById("txt-so-thu-hai").value * 1;
  var soBa = document.getElementById("txt-so-thu-ba").value * 1;

  if (soNhat >= soHai && soNhat >= soBa) {
    if (soHai >= soBa) {
      document.getElementById(
        "result"
      ).innerText = `${soBa}, ${soHai}, ${soNhat}.`;
    } else {
      document.getElementById(
        "result"
      ).innerText = `${soHai}, ${soBa}, ${soNhat}.`;
    }
  } else if (soHai >= soNhat && soHai >= soBa) {
    if (soNhat >= soBa) {
      document.getElementById(
        "result"
      ).innerText = `${soBa}, ${soNhat}, ${soHai}.`;
    } else {
      document.getElementById(
        "result"
      ).innerText = `${soNhat}, ${soBa}, ${soHai}.`;
    }
  } else if (soBa >= soNhat && soBa >= soHai) {
    if (soNhat >= soHai) {
      document.getElementById(
        "result"
      ).innerText = `${soHai}, ${soNhat}, ${soBa}.`;
    } else {
      document.getElementById(
        "result"
      ).innerText = `${soNhat}, ${soHai}, ${soBa}.`;
    }
  }
}

/* bài 2
input: lựa chọn ai đang dùng máy

bước 1: tạo biến cho select
bước 2: lấy dữ liệu người dùng
bước 3: lựa ra value đã chọn

output: gửi lời chào */

function xinChao() {
  var nguoiDung = document.getElementById("txt-nguoi-dung").value;
  switch (nguoiDung) {
    case "B":
      nguoiDung = "Xin chào Bố!";
      break;
    case "M":
      nguoiDung = "Xin chào Mẹ!";
      break;
    case "A":
      nguoiDung = "Xin chào Anh Trai!";
      break;
    case "E":
      nguoiDung = "Xin chào Em Gái!";
      break;
    default:
      nguoiDung = "Vui lòng chọn ai đang dung máy!";
  }
  document.getElementById("result2").innerHTML = nguoiDung;
}

/* bài 3
input: người dùng nhập 3 số nguyên

bước 1: tạo 3 biến 1,2,3
bước 2: lấy dữ liệu người dùng đã nhập
bước 3: chia từng số cho 2 để tính ra chẵn lẽ

output: hiển thị bao nhiêu số chẵn, số lẻ */

function chanLe() {
  var soThuMot = document.getElementById("txt-so-nhat").value * 1;
  var soThuHai = document.getElementById("txt-so-hai").value * 1;
  var soThuBa = document.getElementById("txt-so-ba").value * 1;
  if (soThuMot % 2 == 0 || soThuHai % 2 == 0 || soThuBa % 2 == 0) {
    var soChan1 = soThuMot % 2 == 0;
    var soChan2 = soThuHai % 2 == 0;
    var soChan3 = soThuBa % 2 == 0;
    var tongSoChan = soChan1 + soChan2 + soChan3;
    var tongSoLe = 3 - tongSoChan;
    document.getElementById(
      "result3"
    ).innerText = `Số chẵn: ${tongSoChan}, Số lẻ: ${tongSoLe}.`;
  } else {
    document.getElementById("result3").innerText = "Số chẵn: 0, Số lẽ: 3.";
  }
}

/* bài 4
input: người dùng nhập 3 nguyên cho 3 cạnh

bước 1: tạo 3 biến cho 3 cạnh
bước 2: lấy dữ liệu người dùng
bước 3: xét giá trị các cạch mà người dùng nhập
bước 4: áp dụng công thức để dự đoán hình tam giác

output: dự đoán hình tam giác */

function tamGiac() {
  var canhMot = document.getElementById("txt-canh-mot").value * 1;
  var canhHai = document.getElementById("txt-canh-hai").value * 1;
  var canhBa = document.getElementById("txt-canh-ba").value * 1;
  if (
    canhMot + canhHai >= canhBa &&
    canhHai + canhBa >= canhMot &&
    canhMot + canhBa >= canhHai &&
    canhMot > 0 &&
    canhHai > 0 &&
    canhBa > 0
  ) {
    if (canhMot == canhHai && canhHai == canhBa && canhBa == canhMot) {
      document.getElementById("result4").innerText = `Tam giác đều.`;
    } else if (canhMot == canhHai || canhMot == canhBa || canhHai == canhBa) {
      document.getElementById("result4").innerText = `Tam giác cân.`;
    } else if (
      canhMot * canhMot == canhHai * canhHai + canhBa * canhBa ||
      canhHai * canhHai == canhMot * canhMot + canhBa * canhBa ||
      canhBa * canhBa == canhHai * canhHai + canhMot * canhMot
    ) {
      document.getElementById("result4").innerText = `Tam giác vuông.`;
    } else {
      document.getElementById("result4").innerText = `Tam giác thường.`;
    }
  } else {
    document.getElementById(
      "result4"
    ).innerText = `Một trong 3 cạnh không thoả điều kiện tạo thành hình tam giác!!`;
  }
}

/* bài 5
input: người dùng nhập vào số ngày, tháng, năm

bước 1: tạo 3 biến cho ngày, tháng, năm
bước 2: lấy dữ liệu người dùng
bước 3: xét giá trị ngày, tháng, năm
bước 4: xét giá trị có phải là tháng hay không nếu là tháng 2 thì xét trường hợp có phải là năm nhuận hay không

output: đưa ra kết quả ngày mai hoặc ngày hôm qua */

function ngayTruoc() {
  var soNgay = document.getElementById("txt-so-ngay").value * 1;
  var soThang = document.getElementById("txt-so-thang").value * 1;
  var soNam = document.getElementById("txt-so-nam").value * 1;
  if (
    (soNgay != 1 && soNgay <= 31 && soThang == 1) ||
    (soNgay != 1 && soNgay <= 30 && soThang == 4) ||
    (soNgay != 1 && soNgay <= 30 && soThang == 6) ||
    (soNgay != 1 && soNgay <= 30 && soThang == 9) ||
    (soNgay != 1 && soNgay <= 30 && soThang == 11) ||
    (soNgay != 1 && soNgay <= 31 && soThang == 3) ||
    (soNgay != 1 && soNgay <= 31 && soThang == 5) ||
    (soNgay != 1 && soNgay <= 31 && soThang == 7) ||
    (soNgay != 1 && soNgay <= 31 && soThang == 8) ||
    (soNgay != 1 && soNgay <= 31 && soThang == 10) ||
    (soNgay != 1 && soNgay <= 31 && soThang == 12)
  ) {
    soNgay = soNgay - 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}.`;
  } else if (
    (soNgay == 1 && soNgay <= 31 && soThang == 5) ||
    (soNgay == 1 && soNgay <= 31 && soThang == 7) ||
    (soNgay == 1 && soNgay <= 31 && soThang == 10) ||
    (soNgay == 1 && soNgay <= 31 && soThang == 12)
  ) {
    soNgay = 31 - 1;
    soThang = soThang - 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else if (
    (soNgay == 1 && soNgay <= 29 && soThang == 2) ||
    (soNgay == 1 && soNgay <= 30 && soThang == 4) ||
    (soNgay == 1 && soNgay <= 30 && soThang == 6) ||
    (soNgay == 1 && soNgay <= 31 && soThang == 8) ||
    (soNgay == 1 && soNgay <= 30 && soThang == 9) ||
    (soNgay == 1 && soNgay <= 30 && soThang == 11)
  ) {
    soNgay = 32 - 1;
    soThang = soThang - 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else if (soNgay == 1 && soThang == 1) {
    soNgay = 32 - 1;
    soThang = 13 - 1;
    soNam = soNam - 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else if (soNam % 4 == 0 && soThang == 2 && soNgay != 1 && soNgay == 29) {
    soNgay = soNgay - 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else if ((soThang == 2 && soNgay <= 28) || soNam % 4 == 0) {
    soNgay = soNgay - 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else {
    document.getElementById("result5").innerText = `Không Phải Năm Nhuận. `;
  }
}
function ngaySau() {
  var soNgay = document.getElementById("txt-so-ngay").value * 1;
  var soThang = document.getElementById("txt-so-thang").value * 1;
  var soNam = document.getElementById("txt-so-nam").value * 1;
  if (
    (soNgay != 1 && soNgay < 31 && soThang == 1) ||
    (soNgay != 1 && soNgay < 30 && soThang == 4) ||
    (soNgay != 1 && soNgay < 30 && soThang == 6) ||
    (soNgay != 1 && soNgay < 30 && soThang == 9) ||
    (soNgay != 1 && soNgay < 30 && soThang == 11) ||
    (soNgay != 1 && soNgay < 31 && soThang == 3) ||
    (soNgay != 1 && soNgay < 31 && soThang == 5) ||
    (soNgay != 1 && soNgay < 31 && soThang == 7) ||
    (soNgay != 1 && soNgay < 31 && soThang == 8) ||
    (soNgay != 1 && soNgay < 31 && soThang == 10) ||
    (soNgay != 1 && soNgay < 31 && soThang == 12)
  ) {
    soNgay = soNgay + 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}.`;
  } else if (
    (soNgay == 31 && soThang == 1) ||
    (soNgay == 31 && soThang == 3) ||
    (soNgay == 31 && soThang == 5) ||
    (soNgay == 31 && soThang == 7) ||
    (soNgay == 31 && soThang == 8) ||
    (soNgay == 31 && soThang == 10)
  ) {
    soNgay = soNgay - 30;
    soThang = soThang + 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}.`;
  } else if (soNgay == 31 && soThang == 12) {
    soNgay = soNgay - 30;
    soThang = soThang - 11;
    soNam = soNam + 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else if (soNam % 4 == 0 && soThang == 2 && soNgay != 1 && soNgay < 29) {
    soNgay = soNgay + 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else if (soNam % 4 == 0 && soThang == 2 && soNgay != 1 && soNgay == 29) {
    soNgay = soNgay - 28;
    soThang = soThang + 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else if ((soThang == 2 && soNgay <= 27) || soNam % 4 == 0) {
    soNgay = soNgay + 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else if ((soThang == 2 && soNgay == 28) || soNam % 4 == 0) {
    soNgay = soNgay - 27;
    soThang = soThang + 1;
    document.getElementById(
      "result5"
    ).innerText = `${soNgay}/${soThang}/${soNam}. `;
  } else {
    document.getElementById("result5").innerText = `Không Phải Năm Nhuận.`;
  }
}

/* bài 6
input: người dùng nhập tháng và nhập năm

bước 1: tạo 2 biến cho tháng và năm
bước 2: lấy dữ liệu người dùng
bước 3: xét tháng 1,3,5,7,8,10,12 sẽ là 31 ngày , 4,5,9,11 sẽ là 30 ngày
bước 4: tháng 2 sẽ tính về năm những năm chia hết cho 4 và không chưa hết cho 100 sẽ là 29 ngày

output: cho ngày */

function tinhNgay() {
  var thang = document.getElementById("txt-thang").value * 1;
  var nam = document.getElementById("txt-nam").value * 1;

  if (
    thang == 1 ||
    thang == 3 ||
    thang == 5 ||
    thang == 7 ||
    thang == 8 ||
    thang == 10 ||
    thang == 12
  ) {
    document.getElementById("result6").innerText = `Có 31 ngày.`;
  } else if (thang == 4 || thang == 6 || thang == 9 || thang == 11) {
    document.getElementById("result").innerText = `Có 30 ngày.`;
  } else if (thang == 2 && nam % 4 == 0) {
    document.getElementById("result6").innerText = `Có 29 ngày.`;
  } else {
    document.getElementById("result6").innerText = `Có 28 ngày.`;
  }
}

/* bài 7
input: người dùng nhập một số nguyên có ba chữ số

bước 1: tạo biến cho baChuSo
bước 2: lấy dữ liệu người dùng
bước 3: tính hàng đơn vị, hàng chục và hàng trăm
bước 4: dùng switch để đổi số sang chữ

output: đọc chữ số người dùng đã nhập */

function docSo() {
  var baChuSo = document.getElementById("txt-ba-chu-so").value * 1;
  var donVi = baChuSo % 10;
  var hangChuc = Math.floor(baChuSo / 10) % 10;
  var hangTram = Math.floor(baChuSo / 100);

  if (baChuSo >= 1000) {
    return alert("Dữ liệu không hợp lệ!!");
  }
  switch (donVi) {
    case 0:
      donVi = "";
      break;
    case 1:
      donVi = "Một";
      break;
    case 2:
      donVi = "Hai";
      break;
    case 3:
      donVi = "Ba";
      break;
    case 4:
      donVi = "Bốn";
      break;
    case 5:
      donVi = "Năm";
      break;
    case 6:
      donVi = "Sáu";
      break;
    case 7:
      donVi = "Bảy";
      break;
    case 8:
      donVi = "Tám";
      break;
    case 9:
      donVi = "chín";
    default:
      alert("Quá số!!");
  }
  switch (hangChuc) {
    case 0:
      hangChuc = "Mươi";
      break;
    case 1:
      hangChuc = "Lẻ";
      break;
    case 2:
      hangChuc = "Hai Mươi";
      break;
    case 3:
      hangChuc = "Ba Mươi";
      break;
    case 4:
      hangChuc = "Bốn Mươi";
      break;
    case 5:
      hangChuc = "Năm Mươi";
      break;
    case 6:
      hangChuc = "Sáu Mươi";
      break;
    case 7:
      hangChuc = "Bảy Mươi";
      break;
    case 8:
      hangChuc = "Tám Mươi";
      break;
    case 9:
      hangChuc = "Chín Mươi";
      break;
    default:
      alert("Quá số!!");
  }
  switch (hangTram) {
    case 1:
      hangTram = "Một Trăm";
      break;
    case 2:
      hangTram = "Hai Trăm";
      break;
    case 3:
      hangTram = "Ba Trăm";
      break;
    case 4:
      hangTram = "Bốn Trăm";
      break;
    case 5:
      hangTram = "Năm Trăm";
      break;
    case 6:
      hangTram = "Sáu Trăm";
      break;
    case 7:
      hangTram = "Bảy Trăm";
      break;
    case 8:
      hangTram = "Tám Trăm";
      break;
    case 9:
      hangTram = "Chín Trăm";
      break;
    default:
      alert("Quá số!!");
  }
  document.getElementById(
    "result7"
  ).innerText = `${hangTram} ${hangChuc} ${donVi}.`;
}

/* bài 8
input: người dùng nhập các thông tin sinh viên

bước 1: tạo các biến cho sinh viên
bước 2: lấy dữ liệu sinh viên
bước 3: áp dụng công thức d = Math.pow(x4 - ... ,2 ) + Math.pow(y4 - ..., 2 )
bước 4: so sánh theo công thức toán 3 ngôi

output: hiển thị ai là người xa nhất */

function tim() {
  var name1 = document.getElementById("txt-ten1").value;
  var name2 = document.getElementById("txt-ten2").value;
  var name3 = document.getElementById("txt-ten3").value;
  var x1 = document.getElementById("txt-x1").value * 1;
  var x2 = document.getElementById("txt-x2").value * 1;
  var x3 = document.getElementById("txt-x3").value * 1;
  var x4 = document.getElementById("txt-x4").value * 1;
  var y1 = document.getElementById("txt-y1").value * 1;
  var y2 = document.getElementById("txt-y2").value * 1;
  var y3 = document.getElementById("txt-y3").value * 1;
  var y4 = document.getElementById("txt-y4").value * 1;

  var a = Math.pow(x4 - x1, 2) + Math.pow(y4 - y1, 2);
  d = Math.sqrt(a);
  console.log("🚀 ~ file: index.js:471 ~ tim ~ d", d);
  var b = Math.pow(x4 - x2, 2) + Math.pow(y4 - y2, 2);
  f = Math.sqrt(b);
  console.log("🚀 ~ file: index.js:474 ~ tim ~ f", f);
  var c = Math.pow(x4 - x3, 2) + Math.pow(y4 - y3, 2);
  g = Math.sqrt(c);
  console.log("🚀 ~ file: index.js:477 ~ tim ~ g", g);

  var v = "";
  v = d > f && d > g ? name1 : f > d && f > g ? name2 : name3;
  document.getElementById("result8").innerText = `Sinh viên xa nhất: ${v}.`;
}
